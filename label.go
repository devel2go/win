/* label.go */

package win

import "gitlab.com/devel2go/w32"

type GoLabelControl struct {
	goWidget
	goObject
	text string
	align uint
}

func (l *GoLabelControl) SetAlignment(align uint) (old_align uint) { //  *GoLabelControl
	old_align = l.align
	l.align = align
	if l.hWnd != 0 {
		style := uint(w32.GetWindowLongPtr(l.hWnd, w32.GWL_STYLE))
		style = style &^ w32.SS_LEFT &^ w32.SS_CENTER &^ w32.SS_RIGHT
		w32.SetWindowLongPtr(l.hWnd, w32.GWL_STYLE, uintptr(style | l.align))
		w32.InvalidateRect(l.hWnd, nil, true)
	}
	return old_align
}

/*func (l *GoLabelControl) SetAlignment(align uint) {
	SetTextAlign(hdc HDC, alignment uint)
}*/

/*func (l *GoLabelControl) SetLeftAlign() *GoLabel {
	return l.setAlign(w32.SS_LEFT)
}

func (l *GoLabelControl) SetCenterAlign() *GoLabel {
	return l.setAlign(w32.SS_CENTER)
}

func (l *GoLabelControl) SetRightAlign() *GoLabel {
	return l.setAlign(w32.SS_RIGHT)
}*/

func (l *GoLabelControl) Text() string {
	if l.hWnd != 0 {
		l.text = w32.GetWindowText(l.hWnd)
	}
	return l.text
}

func (l *GoLabelControl) SetText(text string) {
	l.text = text
	if l.hWnd != 0 {
		// TODO this does not work after closing a dialog window with a Label
		w32.SetWindowText(l.hWnd, text)
	}
}

func (l *GoLabelControl) destroy() {
	/*for id, control := range l.controls {
		delete(l.controls, id)
	}*/
}

func (l *GoLabelControl) wid() (*goWidget) {
	return &l.goWidget
}